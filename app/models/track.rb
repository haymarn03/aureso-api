class Track < ActiveRecord::Base
	# check column in validation level
	validates :name, presence: true, uniqueness: true
	validates :time_zone, presence: true

	# calculate slow percentage with slow factors on track
	def slow_rate(slow_factors={})
		slow_factor_percent = 0

		# get slow percentage depend on surface_type
		if slow_factors['surface_type'].present?
			surface_type = slow_factors['surface_type']
			if self.surface_type.include? surface_type
				slow_factor_percent += Random.rand(0..35)
			end
		end

		# get slow percentage depend on current_driving_time with Track.time_zone
		Time.zone = self.time_zone
		current_driving_time = Time.zone.now
		slow_factor_percent += slowrate_by_timeframe(current_driving_time)

		return slow_factor_percent
	end	

	private
	
	# Logic of time_of_the_day with slow factor
	# Time frames 	| Slowing-factor
	# 9am – 6pm 		| 0%
	# 6pm – 9.30pm 	| 8%
	# 9.30pm – 6am 	| 15%
	# 6am – 9am			| 8%
	# params: [driving_time:date_time] 
	# 
	def slowrate_by_timeframe(driving_time)
		nine_am = "09:00AM".in_time_zone(self.time_zone)
		six_pm = "6:00PM".in_time_zone(self.time_zone)
		nine_thirty_pm = "9:30PM".in_time_zone(self.time_zone)
		twelve_am = "12:00AM".in_time_zone(self.time_zone)
		six_am = "6:00AM".in_time_zone(self.time_zone)

  	if nine_am <= driving_time && driving_time <= six_pm
			percentage = 0 
			Rails.logger.info "day time"

		elsif six_pm <= driving_time && driving_time <= nine_thirty_pm
			percentage = 8
			Rails.logger.info "evening time"

		elsif nine_thirty_pm <= driving_time && driving_time <= twelve_am
			percentage = 15
			Rails.logger.info "night time"

		elsif twelve_am <= driving_time && driving_time <= six_am 
			percentage = 15
			Rails.logger.info "mid-night time"

		elsif six_am <= driving_time && driving_time <= nine_am
			percentage = 8
			Rails.logger.info "morning time"

		else
			percentage = 0
			Rails.logger.info  "nothing"
		end
		return percentage
  end

end
