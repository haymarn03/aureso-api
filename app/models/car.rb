class Car < ActiveRecord::Base
	# check column in validation level
	validates :car_slug, presence: true, uniqueness: true
	validates :max_speed, presence: true, numericality: { only_integer: true }

	# calculate max speed with slow factors on car
	def get_max_speed_on_track(slow_factors={})
		# check track first
		track = slow_factors['track']
		@track = Track.where("lower(name) = ?", track.downcase).first

		if @track.present?
			# get slow percentage for car on given track
			slow_rate_percent = @track.slow_rate(slow_factors)
	  	max_speed_on_track = (self.max_speed - (self.max_speed/100.0 * slow_rate_percent)).ceil
	  	return "#{max_speed_on_track} km/h"
		else
			return "track not found"
		end
  end

end
