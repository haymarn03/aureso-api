json.data do
	json.car do
		json.id @car.id
		json.car_slug @car.car_slug.present? ? @car.car_slug : ""
		json.max_speed @car.max_speed.present? ? "#{@car.max_speed} km/h" : ""
		json.max_speed_on_track @max_speed_on_track.present? ? @max_speed_on_track : ""
	end
end