class Api::V1::TracksController < Api::BaseController
	
	def index
		@tracks = Track.all.order(created_at: :desc)
		render json: @tracks, status: 200
	end

	def show
		@track = Track.find_by_id(params[:id])
		if @track.blank?
			return render json: { message: "Recod not found by this id: #{params[:id]}." }, status: 404
		end
		render json: @track, status: 200
	end

	def create
		@track = Track.new(track_params)
		
		if @track.save
      render json: @track, status: 200
		else
			return render json: @track.errors, status: 400
		end
	end

	def update
		@track = Track.find_by_id(params[:id])
		return render json: {message: "Recod not found by this id: #{params[:id]}."}, status: 404 if @track.blank?	

		if @track.update_attributes(track_params)
      render json: @track, status: 200
		else
			return render json: @track.errors, status: 400
		end
	end

	private
	def track_params
      params.permit(:name, :surface_type, :country, :time_zone)
  end

end
