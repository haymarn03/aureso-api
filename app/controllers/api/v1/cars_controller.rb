class Api::V1::CarsController < Api::BaseController

	def index
		@cars = Car.all.order(created_at: :desc)
		render json: @cars, status: 200
	end

	def show
		return render json: {message: "Enter car_slug."}, status: 400 if params[:car_slug].blank?	
		@car = Car.find_by_car_slug(params[:car_slug])
		
		if @car.blank?
			return render json: { message: "Recod not found by this car_slug: #{params[:car_slug]}." }, status: 404
		end

		factors = {}
		# on track
		if params[:track].blank?
			@max_speed_on_track = "no track selected"

		else params[:track].present?
			factors['track'] = params[:track]
			
			# surface_type factor
			factors['surface_type'] = params[:surface_type] if params[:surface_type].present?
			Rails.logger.info "factors: #{factors}"
			
			# calculate max speed on track by using slow factors
			@max_speed_on_track = @car.get_max_speed_on_track factors
		end

		render json: @car, adapter: :json_api

		# render json: @car, serializer: CustomCarSerializer, max_speed_on_track: @max_speed_on_track
	end

	def create
		@car = Car.new(car_params)
		if @car.save
      render json: @car, status: 200
		else
			return render json: @car.errors, status: 400
		end
	end

	def update
		@car = Car.find_by_id(params[:id])
		return render json: {message: "Recod not found by this id: #{params[:id]}."}, status: 404 if @car.blank?	

		if @car.update_attributes(car_params)
			render json: @car, status: 200
		else
			return render json: @car.errors, status: 400
		end
	end

	private
	def car_params
    params.permit(:car_slug, :max_speed)
  end

end
