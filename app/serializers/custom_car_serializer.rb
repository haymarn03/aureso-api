class CustomCarSerializer < ActiveModel::Serializer
  attributes :data

  def data
  	object.car_slug.present? ? object.car_slug : ""
  end

end