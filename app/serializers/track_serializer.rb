class TrackSerializer < ActiveModel::Serializer
  attributes :id, :name, :country, :time_zone, :created_at, :updated_at

  def name
  	object.name.present? ? object.name : ""
  end

  def country
  	object.country.present? ? object.country: ""
  end

  def time_zone
  	object.time_zone.present? ? object.time_zone: ""
  end

  def created_at
  	object.created_at.present? ? object.created_at.strftime("%Y-%m-%d %H:%M:%S") : ""
  end

  def updated_at
  	object.updated_at.present? ? object.updated_at.strftime("%Y-%m-%d %H:%M:%S") : ""
  end

end