class CarSerializer < ActiveModel::Serializer
  attributes :id, :car_slug, :max_speed, :created_at, :updated_at

  def car_slug
  	object.car_slug.present? ? object.car_slug : ""
  end

  def max_speed
  	object.max_speed.present? ? "#{object.max_speed}km/h" : ""
  end

  def created_at
  	object.created_at.present? ? object.created_at.strftime("%Y-%m-%d %H:%M:%S") : ""
  end

  def updated_at
  	object.updated_at.present? ? object.updated_at.strftime("%Y-%m-%d %H:%M:%S") : ""
  end

end