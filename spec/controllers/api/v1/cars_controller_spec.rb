require 'rails_helper'

describe Api::V1::CarsController, :type => :controller do

  # Test case for CarsController#create
  describe 'POST #create' do
    context 'with valid attributes' do
      it 'create car' do
        post :create, car_slug: 'subaru_crosstrek', max_speed: '300', format: :json
        expect(response).to have_http_status(200)
        expect(response.content_type).to eq "application/json"
      end
    end

    context 'duplicate car_slug' do
      it 'create car' do
        post :create, car_slug: 'subaru_impreza', max_speed: '300', format: :json
        expect(response).to have_http_status(400)
      end
    end

    context 'empty car_slug' do
      it 'create car' do
        post :create, car_slug: '', max_speed: '300', format: :json
        expect(response).to have_http_status(400)
      end
    end

    context 'with empty max_speed' do
      it 'create car' do
        post :create, car_slug: 'subaru_crosstrek', max_speed: '', format: :json
        expect(response).to have_http_status(400)
      end
    end
  end

  # Test case for CarsController#show
  describe 'GET #show by filters' do
    context 'no include car_slug' do
      it 'return car object by json' do
        get :show, car_slug: "", format: :json

        expect(response).to have_http_status(400)
      end
    end

    context 'include car_slug' do
      it 'return car object by json' do
        get :show, car_slug: "subaru_impreza", format: :json

        expect(response).to have_http_status(200)
      end
    end

    context 'no include track' do
      it 'return car object by json' do
        get :show, car_slug: "subaru_impreza", format: :json

        expect(assigns(:max_speed_on_track)).to eq("no track selected")
        expect(response).to have_http_status(200)
      end
    end

    context 'invalid track' do
      it 'return car object by json' do
        get :show, car_slug: "subaru_impreza", track: "invalid_track", format: :json

        expect(assigns(:max_speed_on_track)).to eq("track not found")
        expect(response).to have_http_status(200)
      end
    end

    context 'valid track' do
      it 'return car object by json' do
        get :show, car_slug: "subaru_impreza", track: "nurburgring", format: :json

        expect(assigns(:max_speed_on_track)).not_to eq("no track selected")
        expect(assigns(:max_speed_on_track)).not_to eq("track not found")
        expect(response).to have_http_status(200)
      end
    end

    context 'no surface_type' do
      it 'return car object by json' do
        get :show, car_slug: "subaru_impreza", track: "nurburgring", surface_type: "", format: :json

        expect(assigns(:max_speed_on_track)).not_to eq("no track selected")
        expect(assigns(:max_speed_on_track)).not_to eq("track not found")
        expect(response).to have_http_status(200)
      end
    end

    context 'surface_type' do
      it 'return car object by json' do
        get :show, car_slug: "subaru_impreza", track: "nurburgring", surface_type: "snow", format: :json

        expect(assigns(:max_speed_on_track)).not_to eq("no track selected")
        expect(assigns(:max_speed_on_track)).not_to eq("track not found")
        expect(response).to have_http_status(200)
      end
    end

  end

  # Test case for CarsController#update
  describe 'PUT #update' do
    context 'with valid attributes' do
      it 'update car' do
        put :update, id: '1', car_slug: 'test_car_slug', max_speed: 300, format: :json
        expect(response).to have_http_status(200)
        expect(response.content_type).to eq "application/json"
      end
    end

    context 'update by invalid id 0' do
      it 'update car' do
        put :update, id: '0', car_slug: 'test_car_slug', max_speed: 300, format: :json
        expect(response).to have_http_status(404)
        expect(response.content_type).to eq "application/json"
      end
    end

    context 'update by empty car_slug' do
      it 'update track' do
        put :update, id: '2', car_slug: '', max_speed: 300, format: :json
        expect(response).to have_http_status(400)
        expect(response.content_type).to eq "application/json"
      end
    end

    context 'update by duplicate car_slug' do
      it 'update car' do
        put :update, id: '2', car_slug: 'subaru_impreza', max_speed: 300, format: :json
        expect(response).to have_http_status(400)
        expect(response.content_type).to eq "application/json"
      end
    end

    context 'update by empty max_speed' do
      it 'update track' do
        put :update, id: '2', car_slug: 'test_track', max_speed: nil, format: :json
        expect(response).to have_http_status(400)
        expect(response.content_type).to eq "application/json"
      end
    end

  end

end
