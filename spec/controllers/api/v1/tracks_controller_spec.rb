require 'rails_helper'

describe Api::V1::TracksController, :type => :controller do

  # Test case for TracksController#create
  describe 'POST #create' do
    context 'with valid attributes' do
      it 'create track' do
        post :create, name: 'test_track', country: 'Germany', time_zone: 'CET', format: :json
        expect(response).to have_http_status(200)
        expect(response.content_type).to eq "application/json"
      end
    end

    context 'with empty attributes' do
      it 'create track' do
        post :create, name: '', country: '', time_zone: '', format: :json
        expect(response).to have_http_status(400)
        expect(response.content_type).to eq "application/json"
      end
    end
  end

  # Test case for TracksController#show
  describe 'GET #show by id' do
    context 'no id' do
      it 'return track object by json' do
        get :show, id: "", format: :json
        expect(response).to have_http_status(404)
      end
    end

    context 'invalid id' do
      it 'return track object by json' do
        get :show, id: "00", format: :json
        expect(response).to have_http_status(404)
      end
    end

    context 'include id' do
      it 'return track object by json' do
        get :show, id: "2", format: :json
        expect(response).to have_http_status(200)
      end
    end
    
  end

  # Test case for TracksController#update
  describe 'PUT #update' do
    context 'with valid attributes' do
      it 'update track' do
        put :update, id: '1', name: 'test_track', country: 'Germany', time_zone: 'CET', format: :json
        expect(response).to have_http_status(200)
        expect(response.content_type).to eq "application/json"
      end
    end

    context 'update by invalid id 0' do
      it 'update track' do
        put :update, id: '0', name: 'test_track', country: 'Germany', time_zone: 'CET', format: :json
        expect(response).to have_http_status(404)
        expect(response.content_type).to eq "application/json"
      end
    end

    context 'update by duplicate track name' do
      it 'update track' do
        put :update, id: '2', name: 'Circuit Gilles Villenaeuve Montreal', country: 'Australia', time_zone: 'CET', format: :json
        expect(response).to have_http_status(400)
        expect(response.content_type).to eq "application/json"
      end
    end

    context 'update by empty time_zone' do
      it 'update track' do
        put :update, id: '2', name: 'Circuit Gilles Villenaeuve Montreal', country: 'Australia', time_zone: '', format: :json
        expect(response).to have_http_status(400)
        expect(response.content_type).to eq "application/json"
      end
    end

    context 'update by empty time_zone' do
      it 'update track' do
        put :update, id: '2', name: 'test_track', country: 'Australia', time_zone: '', format: :json
        expect(response).to have_http_status(400)
        expect(response.content_type).to eq "application/json"
      end
    end

  end

end
