# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Car.create!(car_slug: "subaru_impreza", max_speed: "280")
Car.create!(car_slug: "mitsubishi_lancer", max_speed: "200")
Car.create!(car_slug: "subaru_wrx", max_speed: "250")
Car.create!(car_slug: "subaru_forester", max_speed: "260")

Track.create!(name: "Nurburgring", surface_type: "{snow,gravel,asphalt}Germany", country: "Germany", time_zone: "CET")
Track.create!(name: "Sydney Motorsport Park", surface_type: "{gravel,asphalt}", country: "Australia", time_zone: "Australia/Lindeman")
Track.create!(name: "Guia Circut", surface_type: "{asphalt}", country: "Macau", time_zone: "Asia/Macau")
Track.create!(name: "Circuit Gilles Villenaeuve Montreal", surface_type: "{snow,gravel,asphalt}", country: "Canada", time_zone: "America/Montreal")
