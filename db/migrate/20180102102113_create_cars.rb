class CreateCars < ActiveRecord::Migration
  def change
    create_table :cars do |t|
      t.string :car_slug, 				null: false, default: ""
      t.integer :max_speed
      t.integer :max_speed_on_track

      t.timestamps null: false
    end

    add_index :cars, :car_slug,				unique: true
  end
end
