class AddColumnIntoTrackTbl2 < ActiveRecord::Migration
  def up
  	add_column :tracks, :country, :string
  	add_column :tracks, :time_zone, :string
  end

  def down
  	remove_column :tracks, :country
  	remove_column :tracks, :time_zone
  end
end
