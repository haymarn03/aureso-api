class RemoveMaxSpeedOnTrackFormCarTbl1 < ActiveRecord::Migration
  def up
  	remove_column :cars, :max_speed_on_track
  end

  def down
  	add_column :cars, :max_speed_on_track, :integer
  end
end
