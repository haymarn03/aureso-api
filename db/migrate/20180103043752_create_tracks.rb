class CreateTracks < ActiveRecord::Migration
  def change
    create_table :tracks do |t|
      t.string :name, 				null: false, default: ""
      t.text :surface_type, 		array: true, default: []

      t.timestamps null: false
    end

    add_index :tracks, :name,				unique: true
  end
end
