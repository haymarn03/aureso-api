Rails.application.routes.draw do
  ActiveAdmin.routes(self)
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root "admin/dashboard#index"

  namespace :api do
  	namespace :v1 do
  		resources :cars, only: [:index, :create, :update] do
        get ':car_slug', to: 'cars#show', on: :collection
      end
      resources :tracks, only: [:index, :show, :create, :update]
  		resources :countries, only: [:index]
  	end
  end
  
end
